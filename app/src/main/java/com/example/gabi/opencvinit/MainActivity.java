
package com.example.gabi.opencvinit;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import opencv_custom.JavaCameraView2;

public class MainActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {
    private static String TAG = "MainActivity";
    JavaCameraView2 jvc2; //Uso esto para encender el flash...
    boolean flash; //Uso esto para ver si el flash esta encendido o no ...

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    private CameraBridgeViewBase mOpenCvCameraView;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called esdfgate");
        flash = false;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        mOpenCvCameraView = (JavaCameraView) findViewById(R.id.HelloOpenCvView);
        mOpenCvCameraView.setMaxFrameSize(640,480);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        jvc2 = (JavaCameraView2) findViewById(R.id.HelloOpenCvView);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getActionBar().setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    private Mat grayMat;
    private Mat rgba;
    private Mat hsvImage;
    private Mat blurredImage;
    private Mat mask;
    private Mat morphOutput;
    private Mat hierarchy;
    private Mat element;
    private Mat image32S;
    private List<MatOfPoint> contours;
    private List<MatOfPoint> matchedContours;


    public void onCameraViewStarted(int width, int height) {
        hsvImage = new Mat();
        blurredImage = new Mat();
        mask = new Mat();
        morphOutput = new Mat();
        hierarchy = new Mat();
        rgba = new Mat();
        grayMat = new Mat();
        image32S = new Mat();
        contours = new ArrayList<MatOfPoint>();
        matchedContours = new ArrayList<MatOfPoint>();

        if (flash == false) {
            Log.e(TAG, "onCameraFrame: Entre al if q enciende flash");
            flash = true;
            jvc2.setupCameraFlashLightOn();
        }


    }

    public void onCameraViewStopped() {
        hsvImage.release();
        blurredImage.release();
        mask.release();
        morphOutput.release();
        hierarchy.release();
        rgba.release();
        grayMat.release();
        image32S.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

//        r
//gba = inputFrame.rgba();
//        // remove some noise
//        Imgproc.GaussianBlur(rgba, blurredImage, new Size(7, 7), 0);
//        //Imgproc.blur(rgba, blurredImage, new Size(7, 7));
//
//        // convert the frame to HSV
//        Imgproc.cvtColor(blurredImage, hsvImage, Imgproc.COLOR_BGR2HSV_FULL);
//
//        // get thresholding values from the UI
//        // remember: H ranges 0-180, S and V range 0-255
//        Scalar minValues = new Scalar(0, 150, 60);
//        Scalar maxValues = new Scalar(29, 255, 255);
//
//        // threshold HSV image to select tennis balls
//        Core.inRange(hsvImage, minValues, maxValues, mask);
//
//        // morphological operators
//        // dilate with large element, erode with small ones
//        Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5));
//        Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5));
//
//        Imgproc.erode(mask, morphOutput, erodeElement);
//        //Imgproc.erode(mask, morphOutput, erodeElement);
//
//        Imgproc.dilate(mask, morphOutput, dilateElement);
//        //Imgproc.dilate(mask, morphOutput, dilateElement);
//
//        // find contours
//        Imgproc.findContours(morphOutput, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);
//
//
//        /// Find the rotated rectangles
//        MatOfPoint2f approxCurve = new MatOfPoint2f();
//
//        for (int i=0; i<contours.size(); i++){
//            if (contours.get(i).size().width > 5 && contours.get(i).size().height > 5){
//                MatOfPoint2f contour2f = new MatOfPoint2f( contours.get(i).toArray() );
//                double approxDistance = Imgproc.arcLength(contour2f, true)*0.02;
//                Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);
//                MatOfPoint points = new MatOfPoint( approxCurve.toArray() );
//                Rect rect = Imgproc.boundingRect(points);
//
//                //error part
//                Imgproc.rectangle(rgba, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height), new Scalar( 0, 0, 255 ),0,8, 0);
//
//            }
//
//        }
//
//        // if any contour exist...
//        if (hierarchy.size().height > 0 && hierarchy.size().width > 0) {
//            // for each contour, display it in blue
//            for (int idx = 0; idx >= 0; idx = (int) hierarchy.get(0, idx)[0]) {
//                Imgproc.drawContours(rgba, contours, idx, new Scalar(255, 0, 0));
//            }
//        } else {
//            Log.d(TAG, "No hay contornos");
//        }

//        return rgba;

        rgba = inputFrame.rgba();

        contours.clear();
        matchedContours.clear();

        Imgproc.blur( inputFrame.rgba(), blurredImage, new Size(7, 7));

        //Convertimos la imagen en gris
        Imgproc.cvtColor(blurredImage,grayMat,Imgproc.COLOR_RGBA2GRAY);
        //Ahora le damos un poco de blur(desenfocado)
        Imgproc.GaussianBlur(grayMat,grayMat,new Size(7,7),0);

        Imgproc.Canny(grayMat,grayMat,500, 600, 5, true);
//        Imgproc.Laplacian(grayMat,grayMat, grayMat.depth());
        Imgproc.adaptiveThreshold(grayMat,grayMat,255,Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,Imgproc.THRESH_BINARY,15,-5);



        int dilation_size = 5; // Este parametro era originalmente 5.
        Mat element1 = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2*dilation_size + 1,2*dilation_size+1 ));
        Imgproc.dilate(grayMat,grayMat,element1);

        //Ahora aplico erocion
        int erotion_size = 5; //Este parametro erea originalmente 5
        element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2*erotion_size+1,2*erotion_size+1));
        Imgproc.erode(grayMat,grayMat,element);

        Imgproc.findContours(grayMat,contours, new Mat(),Imgproc.RETR_EXTERNAL,Imgproc.CHAIN_APPROX_SIMPLE);

        //Pör cada contorno me quiero fijar el tamaño para filtrar los chicos
        for (MatOfPoint contour : contours){
            if (Imgproc.contourArea(contour)>1000) //Descarto los chicos
                matchedContours.add(contour);
        }

        //Si no son dos contornos, termino toda la logica aca porque me interesa encontrar dos objetos nada mas
        if (matchedContours.size() != 2) {
            return rgba;//Me voy a la mierda, mostrando la imagen que se capturo sin ningun agregado
            //Estaria bueno mostrar algun texto de que no detecte dos contornos
            //todo agregar leyenda o imagen que diga que no se detecto nada
        }


        MatOfPoint2f approxCurve = new MatOfPoint2f();
        Rect rect;
        MatOfPoint2f contour2f;
        MatOfPoint points;
        ArrayList<Rect> rects = new ArrayList<Rect>();
        int izquierda = 0; //Uso esto como flag para ver cual contorno esta mas a la izq. Si es 0 es el primero y si es 1 es el segundo

        for (int i = 0; i < matchedContours.size(); i++) { //Por cada contorno que encontramos ...


            contour2f = new MatOfPoint2f(matchedContours.get(i).toArray());
            double approxDistance = Imgproc.arcLength(contour2f, true)*0.02;
            Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);
            points = new MatOfPoint(approxCurve.toArray());

            rect = Imgproc.boundingRect(points);
            rects.add(rect);
            if (i == 0) {
                izquierda = rect.x;
            } else {
                if (izquierda < rect.x) {
                    izquierda = 0;
                    Log.w(TAG, "onCameraFrame: Puse izquierda=0");
                } else {
                    izquierda = 1;
                    Log.w(TAG, "onCameraFrame: Puse izquierda=1");
                }

            }


            ////Voy a meter codigo para dibujar circulos en cada punto del contorno
            Point puntos[] = points.toArray();

            for (int j = 0; j < puntos.length; j++) {
                //Por cada punto quiero :
                //Imgproc.circle(rgba, puntos[j], 6, new Scalar(255, 0, 0)); //Esto pinta un circulo en cada punto que hace al contorno
                //Imgproc.putText(rgba,"("+j+")",puntos[j], Core.FONT_HERSHEY_COMPLEX_SMALL,2,new Scalar(255,0,0),4);


            }

            //Dibujamos un cuadrado alrededor de cada contorno y lo enumero...
            //Imgproc.putText(rgba,"["+i+"]",new Point(rect.x,rect.y), Core.FONT_HERSHEY_COMPLEX_SMALL,2,new Scalar(0,0,255),4);
            Imgproc.rectangle(rgba, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height), new Scalar( 0, 0, 255 ),0,20, 0);
        }

        Rect primero;
        Rect segundo;
        if (izquierda == 0) { //Acomodo segun orden izq a derecha
            primero = rects.get(0); //Este es el primero ( el contorno de mas a la izq. )
            segundo = rects.get(1);
        } else {
            primero = rects.get(1);
            segundo = rects.get(0);
        }

        double tamañofijo = 1.6;
        //Aca meto el procesamiento para tomar las medidas del primero
        primero = rects.get(izquierda); //Este es el primero ( el contorno de mas a la izq. )
        double distanciaeu = distanciaEuclideana(new Point(primero.x, primero.y), new Point(primero.x + primero.width, primero.y));
        double pixelsPerMetric = distanciaeu / tamañofijo; //tamañofijo es en centimetros el tamaño del objeto que ya conozco
        Imgproc.putText(rgba, "[" + tamañofijo + "]", new Point(primero.x, primero.y), Core.FONT_HERSHEY_COMPLEX_SMALL, 2, new Scalar(0, 0, 255), 4);

        //Aca meto el procesamiento para tomar las medidas del segundo

        distanciaeu = distanciaEuclideana(new Point(segundo.x, segundo.y), new Point(segundo.x + segundo.width, segundo.y));
        double ancho = distanciaeu / pixelsPerMetric;

        Imgproc.putText(rgba, "Ancho[" + String.format("%.2f", ancho) + "]", new Point(segundo.x, segundo.y), Core.FONT_HERSHEY_COMPLEX_SMALL, 2, new Scalar(0, 0, 255), 4);
/*
        //Ahora debo medir el alto
        distanciaeu = distanciaEuclideana(new Point(segundo.x, segundo.y), new Point(segundo.x, segundo.y + segundo.height));
        double alto = distanciaeu / pixelsPerMetric;
        Imgproc.putText(rgba, "Alto[" + String.format("%.2f", alto) + "]", new Point(segundo.x, segundo.y + segundo.height), Core.FONT_HERSHEY_COMPLEX_SMALL, 2, new Scalar(0, 0, 255), 4);
*/


        //Ahora debo medir el alto
        distanciaeu = distanciaEuclideana(new Point(segundo.x, segundo.y), new Point(segundo.x, segundo.y + segundo.height));
        double alto = distanciaeu / pixelsPerMetric;
        Imgproc.putText(rgba, "Alto[" + String.format("%.2f", alto) + "]", new Point(segundo.x, segundo.y + segundo.height), Core.FONT_HERSHEY_COMPLEX_SMALL, 2, new Scalar(0, 0, 255), 4);



        Imgproc.drawContours(rgba,matchedContours,-1, new Scalar(0,255,0));//Aca pinto los contours en rgba. rgba tiene la imagen original

        return rgba;

//        Imgproc.findContours(grayMat,contours, new Mat(),Imgproc.RETR_EXTERNAL,Imgproc.CHAIN_APPROX_NONE);
//
//        //Pör cada contorno me quiero fijar el tamaño para filtrar los chicos
//        for (MatOfPoint contour : contours){
//            if (Imgproc.contourArea(contour)>1000) //Descarto los chicos
//                matchedContours.add(contour);
//        }
//
//        MatOfPoint2f approxCurve = new MatOfPoint2f();
//
//        for (int i=0; i<matchedContours.size(); i++){
//
//            MatOfPoint2f contour2f = new MatOfPoint2f( matchedContours.get(i).toArray() );
//            double approxDistance = Imgproc.arcLength(contour2f, true)*0.02;
//            Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);
//            MatOfPoint points = new MatOfPoint( approxCurve.toArray() );
//            Rect rect = Imgproc.boundingRect(points);
//
//            Imgproc.rectangle(rgba, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height), new Scalar( 0, 0, 255 ),0,20, 0);
//
//            //drawLine(rgba, rect, 0.5f);
//        }
//
//        Imgproc.drawContours(rgba,matchedContours,-1, new Scalar(0,255,0));//Aca pinto los contours en rgba. rgba tiene la imagen original
//
//
//        return rgba;

    }

    private void drawLine(Mat img, Rect rect, float value){

        double width = (double)(rect.width);
        double height = (double)(rect.height * value);

        Imgproc.line(img, new Point(rect.x, rect.y + height),new Point(rect.x + width, rect.y + height), new Scalar(255,0,0));
    }

    private double distanciaEuclideana(Point p1, Point p2) { //Funcion simple que calcula distancia entre puntos
        return Math.sqrt(Math.pow((p2.x - p1.x), 2) + Math.pow((p2.y - p1.y), 2));
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

    }
}

